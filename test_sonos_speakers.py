import soco
from soco import SoCo
import requests

import sys
# sys.exit()

print("Starting discovery ...")
all_speakers = soco.discover()
print(all_speakers)

print("Printing speaker names ...")
for speaker in all_speakers:
    print(speaker.player_name)

print("Printing speaker info ...")

for speaker in all_speakers:
    speaker_info = speaker.get_speaker_info(refresh = True, timeout = 2)
    print(f"speaker_info: {speaker_info}")

print("Done")
