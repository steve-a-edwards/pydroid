# On Samsung phone this goes in /storage/emulated/0/Documents/pydroid
# This is used by the Pydroid app which allows the running of Python programs.
# The Termux app on the phone has been used to install git.

print("File from gitlab")

# From Pydroid: Open - InternalStorage / Documents / pydroid

# In termux to pull from gitlab:
#   $ cd /storage/emulated/0/Documents/pydroid
#   $ git pull
# git clone already done

# Termux downloaded from PlayStore (not F-Droid)
# https://termux.com/
# Was installed to allow git pull.
# Needed to turn predictive text off, otherwise no echo of chars typed!
# Termux was allowed to have access to a wider range of files / folders:
# https://wiki.termux.com/wiki/Termux-setup-storage
# $ termux-setup-storage
