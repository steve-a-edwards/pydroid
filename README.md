# pydroid

Samsung phone code for use with Pydroid.
Installed on Samsung phone:
- Pydroid 3 (upgraded to premium)
- termux (to allow use of git to pull files for Pydroid)
  - installed git
  - added pull.sh to change dir and do git pull
  - key mappings: https://wiki.termux.com/wiki/Touch_Keyboard
  
